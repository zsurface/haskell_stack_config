{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module AronModule where
import Control.Monad
import Data.Char 
import qualified Data.Set as DS
import Data.List
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import Data.Maybe(fromJust, isJust)
import Data.Foldable (foldrM)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import System.Random
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix

import qualified Data.Text as Text
import qualified Data.Vector as Vec 

-------------------------------------------------------------------------------- 
-- | How to build AronModule.hs 
-- | ghc --make AronModule.hs
-------------------------------------------------------------------------------- 

------------------------------------------------------------------   
-- | Overloading the sin, cos to avoid Haskell crazy strict type 
class Cnum a where
    _sqrt,_sin,_cos::a -> Float
instance Cnum Int where
    _cos n = cos(realToFrac n)
    _sin n = sin(realToFrac n)
    _sqrt n = sqrt(realToFrac n)
instance Cnum Float where
    _cos n = cos n 
    _sin n = sin n 
    _sqrt n = sqrt n 
instance Cnum Integer where
    _cos n = cos (realToFrac n) 
    _sin n = sin (realToFrac n) 
    _sqrt n = sqrt (realToFrac n) 

-- | = Division is painful in Haskell
-- |   Try to overload _div for many types
class Dnum a b where
    type DivType a b
    _div::a->b-> DivType a b

instance Dnum Integer Float where
    type DivType Integer Float = Integer 
    _div a b = div a (round b)  

instance Dnum Float Integer where
    type DivType Float Integer = Integer 
    _div a b = div (round a) b  

instance Dnum Int Float where
    type DivType Int Float = Integer 
    _div a b = div (toInteger a) (round b)  

instance Dnum Float Int where
    type DivType Float Int = Integer 
    _div a b = div (round a) (toInteger b) 

instance Dnum Double Int where
    type DivType Double Int = Integer 
    _div a b = div (round a) (toInteger b) 

instance Dnum Double Integer where
    type DivType Double Integer = Integer 
    _div a b = div (round a) b 

instance Dnum Integer Double where
    type DivType Integer Double  = Integer 
    _div a b = div a (round b)  

class Add a b where
    type SumTy a b
    addd :: a -> b -> SumTy a b

instance Add Integer Double where
    type SumTy Integer Double = Double
    addd x y = fromIntegral x + y

instance Add Double Integer where
    type SumTy Double Integer = Double
    addd x y = x + fromIntegral y

instance Add Double Int where
    type SumTy Double Int = Double
    addd x y = x + fromIntegral y

instance Add Int Double where
    type SumTy Int Double = Double
    addd x y = (fromIntegral x) + y 

instance (Num a) => Add a a where
    type SumTy a a = a
    addd x y = x + y
    

div'::Int->Int->Double
div' n m = (fromInteger(toInteger n)) / (fromInteger(toInteger m)) 

------------------------------------------------------------------   
-- | = simple matrix can be used in GHCi

mat = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 10]
      ]

mat'= [
       ["-2/3" , "-4/3" , "1"], 
       ["-2/3" , "11/3" , "-2"], 
       ["1"    , "-2"   , "1"]
      ]

-- | Quaternion Implementation 
data Quaternion = Quaternion{a::Float, b::Float, c::Float, d::Float} deriving (Show)

instance Num Quaternion where
    (Quaternion a1 b1 c1 d1) + (Quaternion a2 b2 c2 d2) = Quaternion(a1 + a2) (b1 + b2) (c1 + c2) (d1 + d2)
    (Quaternion a1 b1 c1 d1) - (Quaternion a2 b2 c2 d2) = Quaternion(a1 - a2) (b1 - b2) (c1 - c2) (d1 - d2)
    (Quaternion a  b  c  d)  * (Quaternion e  f  g  h)  = Quaternion(a*e - b*f - c*g -e*h) (b*e + a*f -e*g + c*h) (c*e + e*f + a*g -b*h) (d*e -c*f + b*g + a*h) 
    

-- | = Implement complex number operations
data Complex = Complex{x::Float, y::Float} deriving (Show)

instance Num Complex where
        (Complex x1 y1) + (Complex x2 y2) = Complex(x1 + x2) (y1 + y2)
        (Complex x1 y1) - (Complex x2 y2) = Complex(x1 - x2) (y1 - y2)
        (Complex x1 y1) * (Complex x2 y2) = Complex(x1*x2 - y1*y2) (x1*y2 + x2*y2)
        fromInteger n                     = Complex(fromInteger n) 0 
        signum (Complex x y)              = Complex(signum x) (signum y) 
        abs (Complex x y)                 = Complex(abs x) (abs y) 

instance Eq Complex where
        (Complex x y) == (Complex x' y') = x == x' && y == y'
        (Complex x y) /= (Complex x' y') = not (x == y') && not (y == y') 

conjugate::Complex->Complex
conjugate (Complex x y) = Complex x (-y)

magnitude::Complex->Float
magnitude (Complex x y) = abs(sqrt(realToFrac(x*x + y*y))) 

realPart::Complex->Float
realPart (Complex x _) = x

imagPart::Complex->Float
imagPart (Complex _ y) = y

coord::Complex->(Float, Float)
coord (Complex x y) = (x, y)


radianToDegree::Float->Float
radianToDegree x = x*r where r = 360/(2*pi)

degreeToRadian::Float->Float
degreeToRadian x = x*d where d = (2*pi)/360

tri::Complex->(Float, Float, Float)
tri (Complex x y) = (r, cos(x/r), sin(y/r)) where r = sqrt(x*x + y*y)

data GPoint = GPoint Int Int 
getX::GPoint->Int
getX (GPoint x _) = x

getY::GPoint->Int
getY (GPoint _ y) = y 

toUpperStr::String->String 
toUpperStr s = foldr(\x y -> (toUpper x):y) [] s 

cat::[String]->String
cat  [] = [] 
cat (x:xs) = x ++ cat xs

-- | = ["cat", "dog"] "cat" "pig" => ["pig", "dog"]
-- | replace a matching string with other string
replaceList::[String]->String->String->[String]
replaceList [] _ _ = [] 
replaceList (x:xs) p r = subRegex (mkRegex p) x r : replaceList xs p r 

-- | = repeat's 3 "pig" => ["pig", "pig", "pig"]
repeat'::Integer->a->[a]
repeat' n a = map(const a)[1..n] 

-- | = replicate' 4 'a' => "aaaa"
replicate'::Int->a->[a]
replicate' n x = take n (repeat x)

-- cat maybe list
catMaybe::Maybe [Int]-> Maybe [Int] -> Maybe [Int]
catMaybe Nothing Nothing           = Nothing
catMaybe Nothing (Just list)       = Just list
catMaybe (Just list) Nothing       = Just list
catMaybe (Just list1) (Just list2) = Just (list1 ++ list2)

-- remove white space from string
removeSpace::String->String
removeSpace s = filter(\x -> isSpace x == False) s 

-- check if string is empty
isEmpty::String->Bool
isEmpty "" = True 
isEmpty _  = False 

trimWS::String->String
trimWS []  = [] 
trimWS s   = filter(not . isSpace) s 

trim::String->String
trim s  = Text.unpack $ Text.strip $ Text.pack s

-- | split list of string when string is empty/space
-- | ["dog", " ", "", "cat"] => [["dog"], [], ["cat"]]
splitListEmptyLine::[String]->[[String]]
splitListEmptyLine xs = filter(\x-> length x > 0) $ splitWhen(\x -> (length $ trimWS x) == 0) xs 

splitList::[String]->([String], [String])
splitList [] = ([], [])
splitList [x] = ([x], [])
splitList (x:y:xs) = (x:xp, y:yp) where (xp, yp) = splitList xs

-- print matrix 
pa::(Show a)=>[[a]]->IO()
pa x = mapM_ print x

pa1::(Show a)=>[a]->IO()
pa1 x = mapM_ print x

-- Note: print partList n [] will cause error since print needs concrete type 
-- split list to n blocks
-- partList 2 [1, 2, 3, 4, 5] = [[1,2], [3, 4],[5]]
partList ::Int->[a]->[[a]]
partList _ [] = []
partList n xs = (take n xs) : (partList n $ drop n xs)

partList2 ::Int->[a]->[[a]]
partList2 _ [] = []
partList2 n xs = (take n xs) : (partList n $ drop 1 xs)

-- | partition string to [String] according to character class []
-- | r = "[,.]" s = "dog,cat,cow.fox" => ["dog", "cat", "cow", "fox"]
partitionStrRegex::String->String->[String]
partitionStrRegex r s = splitWhen(\x -> matchTest rex (x:[])) s
                where
                    rex = mkRegex r

prefix::String->[String]
prefix s = filter(\x -> length x > 0) $ map(\n -> take n s) [0..length s] 

unique::(Ord a)=>[a]->[a]
unique xs = DS.toList $ DS.fromList xs

-- length of two lists has to be same 
-- [["a"], ["b"]] [["1"], ["2"]] -> [["a", "1"], ["b", "2"], []]
mergeListList::[[String]]->[[String]]->[[String]]
mergeListList [[]] [[y]] = [["", y]] 
mergeListList [[x]] [[]] = [[x, ""]] 
mergeListList [[]] (y:ys) = ([]:y):ys 
mergeListList (x:xs) [[]] = ([]:x):xs
mergeListList (x:xs) (y:ys) = (zipWith(\ex ey -> [ex, ey]) x y) ++ (mergeListList xs ys) 

-- [1, 2, 3] [10, 11] => [1, 10, 2, 11, 3]
mergeList::[a]->[a]->[a]
mergeList _ [] = []
mergeList [] _ = [] 
mergeList (x:xs) (y:ys) = x:y:mergeList xs ys 

-- both list has to be the same length
-- ow. return Nothing
mergeListLen::[a]->[a]->Maybe [a]
mergeListLen [] [] = Just [] 
mergeListLen (x:xs) (y:ys) = 
            case mergeListLen xs ys of
            Just merged -> Just (x:y:merged)
            Nothing  -> Nothing
mergeListLen _ _  = Nothing

-- iterate a list like a for loop or forM_
iterateList::[a]->(a -> IO ()) -> IO ()
iterateList [] f = return ()
iterateList (x:xs) f = f x >> iterateList xs f

unwrap::Maybe a -> a
unwrap Nothing = error "There are some errors, Cruft" 
unwrap (Just x) = x

codeCapture::String->String
codeCapture str = subRegex(mkRegex pat) str rep 
                    where rep = "<cool>\\1</cool>" 
                          pat = "(([^`]|`[^[]]*|\n*)*)"
-- |
-- = There are many versions of quicksort here
-- == bad version 
qqsort::(a->a->Bool)->[a]->[a]
qqsort cmp [] = []
qqsort cmp (x:xs) = qqsort cmp ([ e | e <- xs, cmp e x ]) ++ [x] ++ qqsort cmp [ e | e <- xs, not  (cmp e x)]

-- |
-- == better version 
quickSort::[Int]->[Int]
quickSort [] = []
quickSort [x] = [x]
quickSort l = quickSort(left) ++ [p] ++ quickSort right 
                    where
                        left =  [x | x <- init l, x < p]
                        right = [x | x <- init l, x >= p]
                        p = last l 

-- |
-- == nice version 
quickSort'::[Int]->[Int]
quickSort' [] = []
quickSort' (x:xs) = quickSort' ([ l | l <- xs, l < x]) ++ [x] ++ quickSort' ([ r | r <- xs, r >= x]) 

-- note: quickSort1 [] -- get error
-- print quickSort1 ([]::Int)  -- it works
quickSort1::(Ord a)=>[a]->[a]
quickSort1 [] = [] 
quickSort1 (x:xs) = quickSort1 ([ l | l <- xs, l < x ]) ++ [x] ++ quickSort1 ([ r | r <- xs, r >= x])

mergeSort::[Int]->[Int]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort l = merge(mergeSort left) (mergeSort right) 
                where
                    half = (length l) `div` 2
                    left = take half l
                    right = drop half l  

                    merge::[Int]->[Int]->[Int]
                    merge [] r = r
                    merge l [] = l
                    merge (x:xs) (y:ys)  = if x < y
                                            then 
                                               x:merge xs (y:ys) 
                                            else
                                               y:merge (x:xs) ys 

mergeSortM::[[Integer]]->[[Integer]]
mergeSortM [] = []
mergeSortM [x] = [x]
mergeSortM l = merge(mergeSortM left) (mergeSortM right) 
                where
                    half = (length l) `div` 2
                    left = take half l
                    right = drop half l  

                    merge::[[Integer]]->[[Integer]]->[[Integer]]
                    merge [] r = r
                    merge l [] = l
                    merge (x:xs) (y:ys)  = if not $ moreZero x y 
                                            then 
                                               x:merge xs (y:ys) 
                                            else
                                               y:merge (x:xs) ys 

                    -- | [0, 1] [1] => False
                    -- | [0, 1] [1, 1] => True
                    -- | [0 1 0] [1 0 0] => False
                    moreZero::[Integer]->[Integer]->Bool
                    moreZero [] [] = False
                    moreZero (x:cx) (y:cy) =if length (x:cx) /= length (y:cy) then False 
                                            else 
                                                if x == 0 && x == y then moreZero cx cy 
                                                 else 
                                                    (if x == 0 && y /= 0 then True else False)



-- | read $b/vim/snippet.m file 
-- | partition blocktexts into list
-- | parse the header. e.g. latex_eqq:*.tex: latex equation
-- | return [([header str]), [blocktext])]
readSnippet::FilePath->IO [([String], [String])]
readSnippet path = do 
            list <- readFileToList path;
            let ll = filter(\x -> length x > 0) $ splitWhen(\x -> (length $ trim x) == 0) list
            let plist = map(\x -> ((partitionStrRegex "[,:]" $ head x), x) ) ll
            let first =  map(\x -> fst x) plist
            let second = map(\x -> snd x) plist
            let pplist = map(\k -> (
                                       unique $ foldr(++) [] (map(\x -> map(\r -> trim r) $ partitionStrRegex "[,:]" x) (fst k)), 
                                       snd k
                                   ) 
                            ) plist
            return pplist 

-- | read dir, all dir or files in [String]
-- | ./ => ["f1", "f2"]
readFileToList::FilePath->IO [String] 
readFileToList f = readFile f >>= \contents -> return (lines contents) 

writeToFile::FilePath->[String]->IO()
writeToFile f list = writeFile f $ unlines list 

wfs::FilePath->[String]->IO()
wfs f list = writeToFile f list
  
wf::FilePath->String->IO()
wf = writeFile

writeToFileAppend::FilePath->[String]->IO()
writeToFileAppend f list = appendFile f $ unlines list 

pp::(Show s)=>s->IO()
pp s = print s 

fpp::(Show s)=>String->s->IO()
fpp msg s = fw msg >> pp s 

fl::IO()
fl = print $ foldr (++) "" $ replicate 80 "-" 

ff::(Show var)=>String->var->IO()
ff msg var = putStr (left ++ msg ++ right ++ "\n" ++ show(var) ++ "\n")
                where
                    line = foldr(++) "" $ replicate 80 "-"
                    diff   = 80 - (length msg)
                    half   = div diff 2
                    isEven = mod diff 2
                    left   = foldr(++) "" $ replicate (half + isEven)  "-"
                    right  = foldr(++) "" $ replicate half  "-"

fw::String->IO()
fw msg = putStr (left ++ msg ++ right ++ "\n")
                where
                    line = foldr(++) "" $ replicate 80 "-"
                    diff   = 80 - (length msg)
                    half   = div diff 2
                    isEven = mod diff 2
                    left   = foldr(++) "" $ replicate (half + isEven)  "-"
                    right  = foldr(++) "" $ replicate half  "-"

-- dot product
listDot::(Num a)=>[a]->[a]->a
listDot [] y = 0 
listDot x [] = 0 
listDot x y = sum $ zipWith(*) x y 

-- multiply scale and list 
scaleList::(Num a)=>a -> [a] -> [a]
scaleList _ [] = [] 
scaleList c v  = map(\x -> c*x) v

listScale::(Num a)=>[a] -> a -> [a]
listScale [] _ = [] 
listScale v  c = map(\x -> c*x) v

-- add two lists
listAdd::(Num a)=>[a]->[a]->[a]
listAdd [] _ = []
listAdd _ [] = []
listAdd u v  = zipWith(\x y -> x+y) u v 

listSub::(Num a)=>[a]->[a]->[a]
listSub [] _ = []
listSub _ [] = []
listSub u v  = zipWith(\x y -> x - y) u v 

listMul::(Num a)=>[a]->[a]->[a]
listMul [] _ = []
listMul _ [] = []
listMul u v  = zipWith(\x y -> x * y) u v 

--listDiv::[Integer]->[Integer]->[a]
--listDiv [] _ = []
--listDiv _ [] = []
--listDiv u v  = zipWith(\x y -> (realToFrac x)/ (realToFrac y)) u v 

listNeg::(Num a)=>[a]->[a]
listNeg [] = []
listNeg u  = map(\x -> -x) u

-- | get dimenstion of matrix -> (nrow, ncol)
-- | if [] -> (0, 0)
dim::[[a]]->(Int, Int)
dim  [] = (0, 0)
dim  (x:xs) = (length (x:xs), length x)

removeIndex::Int->[a]->[a]
removeIndex _ [] = []
removeIndex n cx = (fst $ splitAt n cx) ++ (tail $ snd $ splitAt n cx)

removeRowCol::Integer->Integer->[[Integer]]->[[Integer]]
removeRowCol _ _ [] = []
removeRowCol m n cx = transpose $ removeIndex (fromInteger n) $ transpose $ removeIndex (fromInteger m) cx

-- | matrix multiplicaiton in Int 
multiMatInt::[[Int]]->[[Int]]->[[Int]]
multiMatInt a b = [ [sum $ zipWith (*) ar bc | bc <- (transpose b)]  | ar <- a]

-- | matrix multiplication in Double
multiMatDouble::[[Double]]->[[Double]]->[[Double]]
multiMatDouble a b = [ [sum $ zipWith (*) ar bc | bc <- (transpose b)]  | ar <- a]

-- | matrix multiplication in any type Num 
multiMat::(Num a)=>[[a]]->[[a]]->[[a]]
multiMat mat1 mat2= transpose $ map(\m2 -> map(\m1 -> sum $ zipWith(*) m1 m2) mat1) $ transpose mat2

multiMatVec::(Num a)=>[[a]]->[a]->[a]
multiMatVec m v = map(\r -> listDot r v) m

-- | matrix multiplication in String/Rational
multiRatMat::[[String]]->[[String]]->[[String]]
multiRatMat a b = [[sumRatList $ zipWith (multR) ar bc | bc <- (transpose b)]  | ar <- a]

sumRatList::[String]->String
sumRatList cx = foldl(addR) "0/1" cx


-- add rational numbers as String 
addR::String->String->String
addR s1 s2 = cx 
            where
                n1 = splitR s1
                n2 = splitR s2
                nn1 = map(\x -> x*(last n2)) n1 
                nn2 = map(\x -> x*(last n1)) n2 
                nn = ((head nn1) + (head nn2)):(tail nn1)
                list = reduce nn 
                cx = show (head list) ++ "/" ++ show (last list)

-- reduce: 3/1 => 3
red1::String->String
red1 s = if dn == 1 then show nu else s 
        where
            xs = splitR s
            nu = head xs
            dn = last xs

-- reduce: -3/-1 = 3/1, 3/-2 => -3/2
red2::String->String
red2 s = ss 
        where
            xs = splitR s
            nu = head xs
            dn = last xs
            ss = if nu < 0 && dn < 0 then show (-nu) ++ "/" ++ show (-dn) 
                    else (if nu > 0 && dn < 0 then show (-nu) ++ "/" ++ show (-dn) else s)

reduceForm::[[String]]->[[String]]
reduceForm m = map(\r -> map (red1 . red2) r) m 


-- 3/6 => 1/2
reduce::[Integer]->[Integer]
reduce cx = cx' 
            where
                d = gcd (head cx) (last cx) 
                cx' = map(\x -> x `quot` d) cx

stringToInt::String->Integer
stringToInt s = read s::Integer 

intToString::Integer->String
intToString n = show n

splitR::String->[Integer]
splitR s = xx' 
            where 
                xs = splitRegex(mkRegex "/") s
                -- it is integer only:"3" => "3/1"
                xs' = if length xs == 1 then xs ++ ["1"] else xs 
                xx = map(\x -> read x::Integer) xs' 
                xx' = case last xx of
                        0 -> error "denominator cannot be zero"
                        _ -> xx

multR::String->String->String
multR s1 s2 = ss 
            where
                n1 = splitR s1
                n2 = splitR s2
                nn = zipWith(\x y-> x*y) n1 n2
                cx = reduce nn
                ss = show (head cx) ++ "/" ++ show (last cx)

-- "3" ["1, "2"] => ["3", "6"]
multRL::String->[String]->[String]
multRL s cx = map(\x -> multR s x) cx

normR::[String]->[String] ->String
normR [] _ = []
normR _ [] = []
normR x y = foldl(addR) "0/1" $ zipWith(\x y -> multR x y) x y 

-- u project on w
proj::[String]->[String]->[String]
proj w u = multRL (divR (normR w u) (normR w w)) w 

addRL::[String]->[String] ->[String]
addRL [] _ = []
addRL _ [] = []
addRL x y = zipWith(\x y -> addR x y) x y 


divR::String->String->String
divR s1 s2 = multR s1 $ invR s2
        where
            n1 = reduce $ splitR s1
            n2 = reduce $ splitR s2

divR'::Integer->Integer->String
divR' m n = divR m' n' 
        where 
            m' = show m
            n' = show n

invR::String->String
invR s = xx 
            where
                cx = splitR s
                xx = case head cx of
                        0 -> error "denominator can not be zero"
                        _ -> (last cx') ++ "/" ++ (head cx')
                            where
                                cx' = map(\x -> show x) cx


subR::String->String->String
subR s1 s2 = addR s1 $ negR s2

negR::String->String
negR s = multR "-1" s

negList::[String]->[String]
negList cx = map(\x -> negR x) cx 

subRL::[String]->[String]->[String]
subRL cx cs = addRL cx (negList cs)

-- | get the diagonal of the matrix
diag1::(Num a)=>[[a]]->[a]
diag1 (cx:[]) = [head cx] 
diag1 (x:cx) = (head x):diag1(map tail cx)

-- | get upper triangle of matrix
uptri::(Num a)=>[[a]]->[[a]]
uptri (cx:[]) = [cx] 
uptri (x:cx) = x:uptri(map tail cx)

-- | reverse words in string
reverseWord::String->String
reverseWord s = trim $ foldr(\x y-> x ++ " " ++ y) [] $ reverse $ splitRegex(mkRegex " ") s

rw = reverseWord

-- | transpose the matrix
trans::[[a]]->[[a]]
trans ([]:_) = []
trans x = (map head x):trans(map tail x) 

data Vec a = Vec [String] deriving Show
data Rat a = Rat String deriving Show

-- | unicode operator
-- | dot product of two vectors
Vec u ⨂ Vec v = Rat $ normR u v 
-- | addition of two vectors
Vec u ⨁ Vec v = Vec $ addRL u v 
-- | subtraction of two vectors
Vec u ⦵	Vec v = Vec $ subRL u v 
-- | division of two rational numbers
Rat x ⨸	Rat y = Rat $ divR x y 

-- | sort row for list of list
sortRow::[[Integer]]->[[Integer]]
sortRow [] = []
sortRow (x:cx) = (sortRow [s | s <- cx, not $ moreZero s x]) ++ [x] ++ (sortRow [ s | s <- cx, moreZero s x])
        where            
            moreZero::[Integer]->[Integer]->Bool
            moreZero _ [] = False
            moreZero (x:cx) (y:cy) = if length (x:cx) /= length (y:cy) then False 
                                    else 
                                        if x == 0 && x == y then moreZero cx cy 
                                         else 
                                            (if x == 0 && y /= 0 then True else False)


-- | tranform the matrix to echolon/upper triangle form 
-- | Note: the determinant is different than the original matrix
upperTri::[[Integer]]->[[Integer]]
upperTri  []         = []
upperTri  m = (head sm):(upperTri am) 
        where
            -- | Note: quicksort will not work here because it is unstable sort 
            sm = mergeSortM m 
            m1 = init $ map(\x -> head sm) sm 
            m2 = tail $ sm 
            ma = zipWith(\mx my -> 
                            let 
                                hmx = head mx
                                hmy = head my
                            in 
                                if hmx == 0 || hmy == 0 
                                then my 
                                else zipWith(\x y -> hmy*x - hmx*y) mx my
                        ) m1 m2
            am = map (tail) ma


divI::Integer->Integer->Double
divI n m = (fromInteger n) / (fromInteger m) 

-- | Find the invertible matrix, return ([[]], [[]]) if the matrix is singular 
-- | The code does not check whether the matrix is singular or not
-- | I'm sure there is better way to it better
inverse::[[Integer]]->([[Double]], [[String]])
inverse m = if pr == 0 then ([[]], [[]]) else (mb', mc)
        where
            id = ident' $ length m 
            ma = zipWith(\x y -> x ++ y) m id 
            mt = upperTri ma
            pr = foldl(*) 1 [head x | x <- mt] 
            ar = zipWith(\x y -> (replicate x 0) ++ y) [0..] mt 
            pm = map(\x -> partList (length ar) x ) ar 
            m1 = map(\r -> head r) pm
            m2 = map(\r -> last r) pm

            m11= reverse $ map(\x -> reverse x) m1 
            m22= reverse $ map(\x -> reverse x) m2 
            m3 = zipWith(\x y -> x ++ y) m11 m22
            m4 = upperTri m3
            --m4'= map(\r -> map(\x -> divI x   $ toInteger (head r)) r) m4
            m4'= map(\r -> map(\x -> divI x $ head r) r) m4
            mm'= zipWith(\x y -> (replicate x 0) ++ y) [0..] m4' 
            mm = map(\x -> partList (length mm') x) mm' 
            m1'= map(\x -> head x) mm
            m2'= map(\x -> last x) mm
            ma'= map(\x -> reverse x) $ reverse m1'
            mb'= map(\x -> reverse x) $ reverse m2'

            -- Rational representation, e.g. "3/4", "3/1" 
            m4''= map(\r -> map(\x -> divR' x $ toInteger (head r)) r) m4
            mm''= zipWith(\x y -> (replicate x "0") ++ y) [0..] m4'' 
            xm' = map(\x -> partList (length mm'') x) mm'' 
            m1''= map(\x -> head x) xm'
            m2''= map(\x -> last x) xm' 
            ma''= map(\x -> reverse x) $ reverse m1''
            mb''= map(\x -> reverse x) $ reverse m2''
            mc  = reduceForm $ map(\r -> map red1 r) mb''

-- | Multiply all the diagonal elements and check whether the product is zero or not
isInvertible::[[Integer]]->Bool
isInvertible [] = False
isInvertible m = if p == 0 then False else True
            where
                mx = upperTri m
                p  = foldl(*) 1 $ map(\x -> if (length x > 0) then head x else 0 ) mx

-- | Generate identity matrix 
ident::Integer->[[Integer]]
ident n = map(\x -> (takeN x m) ++ [1] ++ (takeN (n-1 - x) m)) [0..n-1] 
        where 
            m = repeat' n 0
            takeN::Integer->[Integer]->[Integer]
            takeN n cx = take (fromIntegral n) cx 

ident'::Int->[[Integer]]
ident' n = map(\x -> (take x m) ++ [1] ++ (take (n-1 - x) m)) [0..n-1] 
        where 
            m = replicate n 0

identS::Integer->[[String]]
identS n = map(\x -> (takeN x m) ++ ["1"] ++ (takeN (n-1-x) m)) [0..n-1] 
        where 
           m = repeat' n "0" 
           takeN::Integer->[String]->[String]
           takeN n cx = take (fromIntegral n) cx 


help::IO()
help = do 
        putStrLn "------------------------------------------"
        putStrLn "run [zip]  [foo]                     => zip -r foo.zip foo"
        putStrLn "run [zip]  [foo] [foo.zip]           => zip -r foo.zip foo"
        putStrLn "run [gz]   [file.txt]                => gzip foo => foo.gz"
        putStrLn "run [tar]  [file.txt]                => tar -czvf foo.tar.gz"
        putStrLn "------------------------------------------"
        putStrLn "run uzip file.txt.zip          => unzip foo.zip foo"
        putStrLn "run ugz  file.txt.gz           => gunzip foo.gz foo"
        putStrLn "run utar file.txt.tar.gz       => file.txt"
        putStrLn "------------------------------------------"
        putStrLn "run grep pattern               => grep --color --include=\"*.java\" -Hnris pattern ."
        putStrLn "run grep \"*.hs\" pattern      => grep --color --include=\"*.hs\"   -Hnris pattern ."
        putStrLn "------------------------------------------"
        putStrLn "run find \"*.hs\"              => find -iname \"*.hs\" -print"
        putStrLn "run find a1                    => find -iname [\"*.hs\"] -print"
        putStrLn "run ssh                        => ssh-keygen -C noname"
        putStrLn "------------------------------------------"

cmd::[String] -> IO () 
cmd x = case x of 
        [op]     -> case op of
                         "h"  -> help

                         "k"  -> createProcess (proc "ssh-keygen" ["-C", "noname"]){ cwd = Just "." } >>= \_ -> return ()
                         "ssh" -> do
                                    print cmd
                                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                                    out <- hGetContents hout 
                                    mapM_ putStrLn $ lines out
                                    where 
                                        p = (shell cmd)
                                            { std_in  = Inherit
                                            , std_out = CreatePipe
                                            , std_err = Inherit
                                            }
                                        cmd = "ssh-keygen -C noname"
                         _    -> putStrLn "Invalid option"
        [op, a1] -> case op of
                         "zip" -> createProcess (proc "/usr/bin/zip"    ["-r", (a1 ++ ".zip"), a1]){ cwd = Just "." } >>= \_ -> return () 
                         "gz"  -> createProcess (proc "/usr/bin/gzip"   [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "tar" -> createProcess (proc "/usr/bin/tar"    ["-czvf", (a1 ++ ".tar.gz"), a1]){ cwd = Just "." } >>= \_ -> return () 
                         "utar"-> createProcess (proc "/usr/bin/tar"    ["-xzvf", a1]){ cwd = Just "." } >>= \_ -> return () 
                         "uzip"-> createProcess (proc "/usr/bin/unzip"  [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "ugz" -> createProcess (proc "/usr/bin/gunzip" [a1]){ cwd = Just "." } >>= \_ -> return () 
                         "find"-> do
                                    print cmd
                                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                                    ec <- waitForProcess ph
                                    out <- hGetContents hout 
                                    mapM_ putStrLn $ lines out
                                    where 
                                        p = (shell cmd)
                                            { std_in  = Inherit
                                            , std_out = CreatePipe
                                            , std_err = Inherit
                                            }
                                        cmd = "/usr/bin/find . -iname \"" ++ a1 ++ "\" -print"
                         "grep"-> do
                                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                                    ec <- waitForProcess ph
                                    out <- hGetContents hout 
                                    mapM_ putStrLn $ lines out
                                    where 
                                        p = (shell $ "grep --color --include=\"*.hs\" -Hnris " ++ a1 ++ " . ")
                                            { std_in  = Inherit
                                            , std_out = CreatePipe
                                            , std_err = Inherit
                                            }
                         _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "]"

        [op, a1, a2] -> case op of
                 "zip" -> createProcess (proc "/usr/bin/zip" ["-r", a2, a1]){ cwd = Just "." } >>= \_ -> return () 
                 "grep"-> do
                            (Nothing, Just hout, Nothing, ph) <- createProcess p
                            ec <- waitForProcess ph
                            out <- hGetContents hout 
                            mapM_ putStrLn $ lines out
                            where 
                                p = (shell $ "grep --color --include=" ++ "\"" ++ a1 ++ "\"" ++ " -Hnris " ++ a2 ++ " . ")
                                    { std_in  = Inherit
                                    , std_out = CreatePipe
                                    , std_err = Inherit
                                    }
                 _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "][" ++ a2 ++ "]"

        _ -> help 


-- | Compile haskell code to $ff/mybin/myHaskell => create symbol link  $sym/sym
-- | [file:myHaskell.hs]  [sym:symbol] link name in $sym
compileHaskellToBin::String->String->IO()
compileHaskellToBin file sym = rm bin >> (run $ toBin file) >> run link >> return ()
                where
                    toBin s = "ghc -i/Users/cat/myfile/bitbucket/haskell -O2 /Users/cat/myfile/bitbucket/haskell/" ++ s ++ " -o " ++ bin
                    link    = "ln -f -s " ++ bin ++ " " ++ symbin
                    symbin  = "/Users/cat/myfile/symbin" </> sym
                    mybin   = "/Users/cat/myfile/mybin"
                    base    = foldr(++) [] $ init $ splitRegex(mkRegex "\\.") file
                    bin     = mybin </> base

-- | Gene random number
drawInt::Int->Int->IO Int
drawInt x y = getStdRandom(randomR(x, y))

randomList::Int->IO [Integer] 
randomList 0 = return []
randomList n = do
        r <- randomRIO (1, 10)
        rs<- randomList (n - 1)
        return (r:rs)
-- | generate m x n random matrix
randomMatrix::Int->Int->IO [[Integer]]
randomMatrix 0 _ = return [] 
randomMatrix _ 0 = return [] 
randomMatrix m n = do
        list <- replicateM m $ randomList n  
        return list 



-- | compare two string ignore cases 
strCompareIC::String->String->Bool
strCompareIC x y = toUpperStr x == toUpperStr y

-- | walk inside the dir
-- | if there is file, then append it to list
-- | otherwise, walking inside the dir 
-- | Pass lambda func: (FilePath -> IO [String]) as argument
-- | 
-- |        let filefunc fname = 
-- |             do if (strCompareIC (takeExtension fname) (toUpperStr ".png"))
-- |                  then return [fname]
-- |                  else return []

dirWalk :: FilePath -> (FilePath -> IO [String]) -> IO [String] 
dirWalk top filefunc = do
  isDirectory <- doesDirectoryExist top
  if isDirectory
    then 
      do
        files <- listDirectory top
        foldrM(\f d -> (dirWalk (top </> f) filefunc >>= \x -> return (x ++ d))) [] files 
    else
      filefunc top

------------------------------------------------------------------ 
-- | Try to replace as many as shell command as possible
-- | See how far I can go
-- | 
-- write many shell commands as possible
-- try to emulate shell commands 
------------------------------------------------------------------ 

ls::IO()
ls = createProcess(proc "ls"  [] ) >> return () 


-- list file in dir
lsFile::String->IO [String]
lsFile s =  do 
            (_, hout, _, _) <- createProcess(proc "ls" [s]){std_out = CreatePipe}
            case hout of
                 Just out -> hGetContents out >>= \x -> return $ lines x 
                 Nothing -> return []

-- return full path files list
-- lsFileFull ["/dog/cat" | "."] 
lsFileFull::String->IO [String]
lsFileFull s =  do 
            cur <- getPwd
            let path = if s == "." then cur else s 
            l <- lsFile path 
            return $ map(\x -> path </> x) l 

-- list file with regex match
lsRegex::String->String->IO [String]
lsRegex s r = lsFile s >>= \f -> return $ filter(matchTest reg) f 
            where
                reg = mkRegexWithOpts r True False


-- list full path with regex match
-- [".", "\\.hs$"] match haskell files only
lsFullRegex::String->String->IO [String]
lsFullRegex s r = lsFileFull s >>= \f -> return $ filter(matchTest reg) f 
            where
                reg = mkRegexWithOpts r True False
-- remove directory recursive
rm::FilePath -> IO()
rm s =  doesFileExist s >>= \x -> if x then (isFile s >>= \x -> if x then removeFile s else return ()) else return ()
                

rmDir::FilePath->IO()
rmDir s = removeDirectoryRecursive s


--removeDirectoryRecursive s 
--isFile::FilePath->IO Bool
--isFile s =  getFileStatus s >>= \st ->return $ isRegularFile st

pwd::IO()
pwd = createProcess(proc "pwd"  [] ) >> return ()

getPwd::IO FilePath
getPwd = getCurrentDirectory

cd::FilePath->IO()
cd p = setCurrentDirectory p 

en::String->IO String
en s = getEnv s 

cc::String->IO ()
cc cmd = callCommand cmd

g::IO()
g = getEnv "g" >>= \x -> print x >> return ()


asplitPath::FilePath -> [String]
asplitPath s =  filter( \x -> length x > 0) $ splitRegex(mkRegex "/") s

pathBase::FilePath -> FilePath
pathBase s = if length list > 0 then last list else [] 
            where
                list = asplitPath s
-- | 1. copy FilePath to "/tmp"
-- | 2. move the file back to dir with newName
-- | copy file and rename it in the same dir
copyRename::FilePath->String->IO()
copyRename fp newName = do
           copyFileToDir fp "/tmp" 
           mv ("/tmp" </> fname) (dir </> newName) 
           rm ("/tmp" </> fname)
           where
                fname = takeFileName fp
                dir = takeDirectory fp

-- if source is valid file and dest is valid dir
-- otherwise error
copyFileToDir::FilePath -> FilePath -> IO()
copyFileToDir s d = copyFile s (d </> pathBase s)

isFile::FilePath->IO Bool
isFile s =  getFileStatus s >>= \st ->return $ isRegularFile st

fExist::FilePath->IO Bool
fExist s = doesFileExist s

fe = fExist

-- | create empty file 
createFile::FilePath->IO()
createFile f = writeFile f [] 

-- | copy dir to other dir
copyDir::FilePath -> FilePath -> IO() 
copyDir s d = do 
                status <- getFileStatus s
                if isRegularFile status then copyFileToDir s d 
                    else do 
                            tmpList <- listDirectory s 
                            let list = map(\x -> s </> x) tmpList
                            fList <- filterM(\x -> getFileStatus x >>= \x -> return $ isRegularFile x) list 
                            dirList <- filterM(\x -> getFileStatus x >>= \x -> return $ isDirectory x) list 
                            let dList = map(\x -> pathBase x) dirList
                            let soList = asplitPath s 
                            let deList = asplitPath d 
                            let full = d </> pathBase s 
                            mkdir full
                            mapM(\x -> copyFile x (full </> (pathBase x)) ) fList 
                            mapM(\x -> copyDir x  full) dirList
                            return () 

-- | rename dir or file => dir or file 
mv::FilePath->FilePath->IO()
mv s d = isFile s >>= \x -> if x then renameFile s d else renameDirectory s d 

rename = mv

-- rename all files in p, e.g. s=XXX img.JPG => img_XXX.JPG
renameAllFile::String->String->IO()
renameAllFile p s = do 
                list <- lsFileFull p 
                mapM(\x -> mv x ((dropExtension x) ++ s ++ (takeExtension x))) list
                return () 

mvFiles = renameAllFile
mvFile = renameFile


mkdir::FilePath -> IO ()
mkdir s = createDirectoryIfMissing False s

mkdirp::FilePath->IO()
mkdirp s = createDirectoryIfMissing True s 

sfilter::String->[String]->[String]
sfilter s l = filter(matchTest $ mkRegex s) l

-- | clear terminal screen
clear = putStr "\ESC[2J"

run::String->IO [String]
run cmd = do 
    (Nothing, Just hout, Nothing, ph) <- createProcess p
    ec <- waitForProcess ph
    hGetContents hout >>= \x -> return $ lines x
    --mapM_ putStrLn $ lines out
    where 
        p = (shell cmd)
            { std_in  = Inherit
            , std_out = CreatePipe
            , std_err = Inherit
            }

-- | Some Math function
-- | derivative of f  
-- | f'(x) = \lim_{h \rightarrow 0} f(x + h) - f(x)/h
df f x = (f(x + h) - f(x))/h where h = 0.001 

-- | draw tangent line at point (x0, f(x0)) for function f
tangentLine f x x0 = (df f x0)*(x - x0) + (f x0)

-- | generate prime number with Sieve Algorithm
prime = sieve [2..]
    where sieve (p:xs) = p:sieve[ x | x <- xs, x `mod` p /= 0]

-- | -------------------------------------------------------------------------------- 
--  Find root for any function
--  
--  Partition the interval [x0, xn] into list = [x0, x1) [x1, x2) [x2, x3) ..[x(n-1), xn)
--  Cocat [xn, xn] with the list since the left boundary is checked only
-- | Note: f(xn) might be zero, we need to check the xn boundary
-- | TODO: need to check f(xn), DONE, concat [xn, xn] to the list
-- | -------------------------------------------------------------------------------- 
-- | Good test cases:
-- | f(x) = x^2 - 4, [-2, 2]
-- | f(x) = x^5 -4*x^4 + 0.1*x^3 + 4*x^2 - 0.5, [-4, 4] → 5 solutions
-- | -------------------------------------------------------------------------------- 
-- | limitation:
-- | if each subinterval contains two or more values, then ONLY one value can be found
-- | subinterval can be adjusted in arbitrary small 
-- | -------------------------------------------------------------------------------- 
-- | [0, 2] (2-0)/2 = [0, 1, 2] = [0, 1) ∪ [1, 2) ∪ [2, 2]
oneRoot::(Double->Double)->Double->Double->Double->Maybe Double 
oneRoot f x0 x1 ε   | abs(f x0)<= ε = Just x0 
                    | abs(f m) <= ε = Just m
                    | f x0 < 0 && f x1 > 0 && f m < 0 = oneRoot f m x1 ε 
                    | f x0 < 0 && f x1 > 0 && f m > 0 = oneRoot f x0 m ε 
                    | f x0 > 0 && f x1 < 0 && f m < 0 = oneRoot f x0 m ε 
                    | f x0 > 0 && f x1 < 0 && f m > 0 = oneRoot f m x1 ε 
                    | otherwise                       = Nothing 
                    where
                        m = (x0 + x1)/2

-- | Find all the roots for a given close interval: [1, 2], 1 or 2 might be the root
-- |
rootList::(Double->Double)->Double->Double->Double->Integer->[Maybe Double] 
rootList f x0 x1 ε n = filter(isJust) $ map(\(t0, t1) -> oneRoot f t0 t1 ε) full 
                where
                    n'   = fromInteger n 
                    list = map(\k ->delt*k + x0) [0..n'] 
                    inte = zipWith(\x y -> (x, y)) (init list) (tail list) 
                    full = inte ++ [(x1, x1)] -- make sure the left boundary is checked
                    delt = abs(x1 - x0)/ n'
